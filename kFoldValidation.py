import hickle as hkl
import tensorflow as tf
import cPickle as pickle
from datetime import datetime

from model_class import convModel

def loadTrainData(kFold=5):
    #load data
    data_folds = {}
    labels_folds = {}
    #load dataset to do kFold validation
    data = hkl.load('model_data.hkl')
    data_folds = data['data_folds']
    labels_folds = data['labels_folds']
    del data#free up space

    #the data isn't too large so load them all into memory for convienience
    train_data = {}
    test_data = {}
    train_labels = {}
    test_labels = {}
    for i in xrange(kFold):
        test_data[i] = data_folds[str(i)]
        test_labels[i] = labels_folds[str(i)]
        for j in xrange(kFold):
            if i != j:
                train_data[i] = data_folds[str(i)]
                train_labels[i] = labels_folds[str(i)]

    #check what the data looks like
    print 'train_data:',train_data[0].shape, type(train_data[0][0,0])
    print 'test_data:',test_data[0].shape
    print 'train_labels:',train_labels[0].shape
    print 'test_labels:',test_labels[0].shape

    return train_data, train_labels, test_data, test_labels

#manually define parameters to use for kFoldValidation
def kFoldGAParams(train_data, train_labels, test_data, test_labels, kFold=5, epochs=10001, fname="GAparamsWide.pickle"):

    try:
        with open(fname, 'rb') as f:
            data = pickle.load(f)
    except Exception as e:
        print e
        print "\nIt seems the parameters file specified: ", fname, " does not exist. but see above for errors."
        return
    ga_params = data['best_params']
    del data

    print 'Genetically optimised parameters :', ga_params
    #do k-fold validation:
    model = convModel(const_params)
    model.setParams(ga_params)
    #note k-fold validation with n=1 is the same as running it once normally
    testAccuracy = model.kFoldValidation(train_data, train_labels, test_data, test_labels, kFold, epochs)
    print 'kFold validation accuracy using genetically optimised parameters: ', testAccuracy.mean()

#manually define parameters to use for kFoldValidation
def kFoldManualParams(train_data, train_labels, test_data, test_labels, kFold=5, epochs=10001):

    manual_params = {
        'batch_size' : 64#5076
        ,'alpha' : 10e-4 #learning_rate
        ,'dropout_prob' : 0.9
        ,'optimizer_type' : tf.train.AdamOptimizer
        ,'depth' : {
            '1': 8
            ,'2': 16
            ,'3': 16
            ,'4': 16
        }
        ,'nodes': {
            'fc1': 256
            ,'fc2': 256
            ,'fc3': 512
        }
    }

    #do kFold validation:
    model = convModel(const_params)
    model.setParams(manual_params)
    #note kFold validation with n=1 is the same as running it once normally
    testAccuracy = model.kFoldValidation(train_data, train_labels, test_data, test_labels, kFold, epochs)
    print 'kFold validation accuracy using manually specified parameters: ', testAccuracy.mean()

if __name__ == '__main__':

    start_time = datetime.now()

    #load training data into memory
    train_data, train_labels, test_data, test_labels = loadTrainData()

    #default parameters:
    const_params = {
        'width' : 200
        ,'height' : 100
        ,'nLabels' : 3
        ,'patch_size' : 5
        ,'stride' : 1
        ,'pstride' : 2
        ,'save_model' : False
        ,'show_fig' : False
        ,'channels' : 1#grayscale
        ,'test_size' : test_labels[0].shape[0]
    }

    ####################################################
    #Run k-fold validation either manually or using GA optimised parameters.
    ####################################################
    #run manually
    #kFoldManualParams(train_data, train_labels, test_data, test_labels, kFold=2, epochs=10000)
    #run using genetically optimised parameters
    kFoldGAParams(train_data, train_labels, test_data, test_labels, kFold=2, epochs=10000)
    ####################################################

    print 'Time taken: ', datetime.now() - start_time
