import pandas as pd
import numpy as np
import cv2
import hickle as hkl
from datetime import datetime

def getImageType(x):
    x = x['url']
    x = x[-7:]
    if '_3' in x:
        return 3
    elif '_2' in x:
        return 2
    else:
        return 1

start_time = datetime.now()

##############################################
##set parameters
##############################################
width = 200
height = 100
nLabels = 3
channels = 1 #grayscale images
imPathPrefix = 'ScrapedCarImages/NewCars/'
dataPath = "NewCarImage.csv"
kFold = 5#For n-fold validation we want to produce multiple train/test splits of the data
##############################################

df = pd.read_csv(dataPath,usecols=[0,2,4,5])

#cleanup column
df['version'] = df['version'].map(lambda x: x.lstrip(' \n').rstrip(' '))

#convert string to dict
df['files'] = df['files'].str.lstrip('[').str.rstrip(']')
df['files'] = df['files'].apply(eval)
#extract data from dictionary
df['imType'] = df['files'].apply(getImageType)
df['path'] = imPathPrefix + df['files'].apply(lambda x:x['path'])
df['imFormat'] = df['path'].apply(lambda x:x[-3:])#want to check that all are jpg

#drop column that is no longer neccessary
df = df.drop('files',axis=1)

def getImages(paths):
    #initialise numpy matrix
    images = np.zeros((paths.shape[0],width*height),np.int32)
    for i in range(paths.shape[0]):
        #read images in.
        #resize so that all images have same dimensions
        #reshape so image is in flat array
        images[i,:] = cv2.resize(
            cv2.imread(paths.iloc[i],cv2.CV_LOAD_IMAGE_GRAYSCALE)
            ,(width,height)).reshape(-1)
    return images

#print out info to see what is happening
print '\ndf.describe\n', df.describe(include='all') #check all are jpg
print '\ndf.head(5)\n', df.head(5)
print '\ndf.shape', df.shape

#get image data
data = getImages(df['path'])
print 'data.shape', data.shape
print data[:2]
#data standardisation
data = (data - np.mean(data))/np.std(data)
print np.mean(data)
print np.std(data)
print data[:2]
#get labels
labels = df.as_matrix(column    s=['imType']).reshape(-1)
print sum(labels)
print 'labels', labels[:2]
#convert labels to one-hot-encodings
one_hot = np.zeros((labels.shape[0],nLabels),np.int32)
one_hot[np.arange(data.shape[0]),labels-1] = 1
labels = one_hot
del one_hot
print 'labels', labels[:2]
print 'labels.shape', labels.shape
print sum(labels[:,0])
print sum(labels[:,1])
print sum(labels[:,2])

#TODO: remove duplicate images

#randomise the data
index = np.arange(labels.shape[0])
np.random.shuffle(index)
data = data[index]
labels = labels[index]

#reformat the data for the model
def reformat(dataset, labels):
  dataset = dataset.reshape(
    (-1, width, height, channels)).astype(np.float32)
  labels = labels.astype(np.float32)
  return dataset, labels
data, labels = reformat(data, labels)


data_folds = {}
labels_folds = {}
foldSize = data.shape[0] // kFold
for i in range(kFold):

    #seperate out into train,test datasets
    if i != kFold:
        data_folds[str(i)] = data[i*foldSize:(i+1)*foldSize]
        labels_folds[str(i)] = labels[i*foldSize:(i+1)*foldSize]
    else:#make the final fold the remaining data
        data_fold[str(i)] = data[i*foldSize:]
        labels_fold[str(i)] = labels[i*foldSize:]

    print 'data_folds[',i,'].shape:', data_folds[str(i)].shape
    print 'labels_folds[',i,'].shape:', labels_folds[str(i)].shape

f = open('model_data.hkl','w')
#store the data to be used in the model
hkl.dump({
    'data_folds': data_folds,
    'labels_folds': labels_folds
},f)
f.close()

print 'Time taken to process data: ', datetime.now() - start_time
