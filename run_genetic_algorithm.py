import cPickle as pickle#hickle didn't want to work nicely for saving parameters
import tensorflow as tf
import numpy as np
from datetime import datetime

from GA_class import GeneticAlgorithm
from kFoldValidation import loadTrainData

if __name__ == '__main__':

    start_time = datetime.now()

    #load data
    train_data, train_labels, test_data, test_labels = loadTrainData()

    #default parameters:
    const_params = {
        'width' : 200
        ,'height' : 100
        ,'nLabels' : 3
        ,'patch_size' : 5
        ,'stride' : 1
        ,'pstride' : 2
        ,'save_model' : False
        ,'show_fig' : False
        ,'channels' : 1#grayscale
        ,'test_size' : test_labels[0].shape[0]
    }

    #define acceptable possible values for each of the parameters:
    varRange = {}
    varRange['batch_size'] = range(40,64)
    varRange['alpha'] = [10e-1, 10e-2, 10e-3, 10e-4, 10e-5, 10e-6, 10e-7]
    varRange['dropout_prob'] = np.linspace(0.3, 1, num=8).tolist()
    varRange['optimizer_type'] = [tf.train.AdamOptimizer
                        ,tf.train.AdadeltaOptimizer
                        ,tf.train.AdagradOptimizer
                        ,tf.train.GradientDescentOptimizer
                        ,tf.train.RMSPropOptimizer]
    varRange['depth'] = range(4,16)
    varRange['nodes'] = range(64,256)

    gaParams = {
        'nPopulation' : 20
        ,'nGenerations' : 20
        ,'keepRatio': 0.4
        ,'nRandKeepChildren': 2
        ,'varRange' : varRange
        ,'const_params' : const_params
        ,'nMutate' : 1
        ,'kFold' : 2
        ,'epochs' : 7501
    }

    gaTestParams = {
        'nPopulation' : 5
        ,'nGenerations' : 2
        ,'keepRatio': 0.4
        ,'nRandKeepChildren': 1
        ,'varRange' : varRange
        ,'const_params' : const_params
        ,'nMutate' : 1
        ,'kFold' : 1
        ,'epochs' : 51
    }

    ####################################################
    #Run GA either with test parameters (short run time to check if code works) OR with actual parameters
    ####################################################
    #ga = GeneticAlgorithm(gaParams)#95.5% fitness runtime:1day, 4hrs, 15mins.
    ga = GeneticAlgorithm(gaTestParams)
    ####################################################

    #initialise the first population
    ga.initPop()
    #check fitness i.e. kFoldValidation of conv model
    ga.evalFitness(train_data, train_labels, test_data, test_labels)
    while ga.GenCounter < ga.nGenerations: #replace with alternate stopping criteria
        ga.breed()
        ga.evalFitness(train_data, train_labels, test_data, test_labels)

    #view the fitnesshistory for debugging purposes
    ga.displayFitnessHistory()

    #write best parameters to file - for later use
    best_params = ga.population[0].getParams()
    with open('GAparamsTest.pickle','wb') as f:
        pickle.dump({'best_params': best_params},f)

    print 'Time taken: ', datetime.now() - start_time
