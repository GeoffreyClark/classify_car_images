import hickle as hkl
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.stats import chisqprob

from model_class import convModel

class GeneticAlgorithm():
    def __init__(self, gaParams):
        self.nPopulation = gaParams['nPopulation'] #the number of models in each generation
        self.nGenerations = gaParams['nGenerations'] #the number of generations to breed before we stop
        self.GenCounter = 0 #initialise to keep track of which generation we are busy with
        self.keepRatio = gaParams['keepRatio'] #the ratio of each generation to keep to breed the next generation
        self.nRandKeepChildren = gaParams['nRandKeepChildren'] #To maintain genetic diversity and avoid early convergence - include a few poor performers among those allowed to breed.
        self.varRange = gaParams['varRange']#a dictionary of the range of values that each variable can take on
        self.const_params = gaParams['const_params']#these are the constant parameters to use to create a new convolutional model
        self.nMutate = gaParams['nMutate']#number of parameters to mutate
        self.epochs = gaParams['epochs']#number of epochs to use in training
        self.kFold = gaParams['kFold']#if we want to use k-fold cross-validation set equal to 2-5 otherwise 1
        self.fitness = [0]*self.nPopulation#need a list to keep track of the fitness of each model: note that if a model is kept from one generation to the next then we will not need to re-evaluate its fitness
        print '\nInitial fitness: ', self.fitness
        self.fitnessHistory = {}#debugging - view the fitness history after the optimiser has run to see how the optimiser performed

    #randomly initialize the population
    def initPop(self):
        self.population = []
        params = {}
        for i in xrange(self.nPopulation):
            #randomly generate the parameters
            params['batch_size'] = np.random.choice(self.varRange['batch_size'])
            params['alpha'] = np.random.choice(self.varRange['alpha'])
            params['dropout_prob'] = np.random.choice(self.varRange['dropout_prob'])
            params['optimizer_type'] = np.random.choice(self.varRange['optimizer_type'])
            params['depth'] = {'1': np.random.choice(self.varRange['depth'])
                                ,'2': np.random.choice(self.varRange['depth'])
                                ,'3': np.random.choice(self.varRange['depth'])
                                ,'4': np.random.choice(self.varRange['depth'])
                                }
            params['nodes'] = { 'fc1': np.random.choice(self.varRange['nodes'])
                                ,'fc2': np.random.choice(self.varRange['nodes'])
                                ,'fc3': np.random.choice(self.varRange['nodes'])
                                }
            #generate the member of the population
            model = convModel(self.const_params)
            model.setParams(params)
            self.population.append(model)
        print '\nPopulation intialised'

    def evalFitness(self, train_data, train_labels, test_data, test_labels):

        for i in range(self.nPopulation):
            #Fitness evaluation is expensive so only evaluate if this is a new member of the population
            if self.fitness[i] == 0:
                #eval the average test accuracy across the kFold cross-validation
                self.fitness[i] = self.population[i].kFoldValidation(train_data, train_labels, test_data, test_labels, K=self.kFold, epochs=self.epochs)
        #sort the lists:
        self.population = [gen for _,gen in sorted(zip(self.fitness, self.population), reverse=True)]
        self.fitness = sorted(self.fitness, reverse=True)

        #for debugging:
        self.fitnessHistory[self.GenCounter] = self.fitness
        print 'Gen: ', self.GenCounter, ' Best fitness: ', self.fitness[0]
        print 'Best parameters: ', self.population[0].getParams()

    def breed(self):
        populationGaps = self.selectParents()

        for i in xrange(len(populationGaps)):
            #only create a child if natural selection has led us to kill this member of the population
            if populationGaps[i]:
                print 'i', i, 'pop gap', populationGaps[i]
                father, mother = self.getParents()
                #create child by combining attributes of the parents:
                child = self.crossover(father, mother)
                #mutate genes to introduce a random element into the population
                child = self.mutate(child)
                #replace previous member of population with new child
                self.population[i] = child

        #increment counter to show that we have moved to the next generation
        self.GenCounter += 1
        print '\nGeneration ', self.GenCounter, ' has been bred'

    def selectParents(self):
        populationGaps = [True] * self.nPopulation#true elements will be killed
        base_keep = int(self.keepRatio * self.nPopulation)#number to keep before we consider lucky losers
        populationGaps[:base_keep] = [False] * base_keep
        #select lucky losers to breed. Maintain genetic diversity: this may help prevent early convergence to local minimum
        #take elements at random:
        lucky = range(self.nPopulation - base_keep)
        np.random.shuffle(lucky)
        lucky = lucky[:self.nRandKeepChildren]
        for i in lucky:
            populationGaps[base_keep + i] = False

        #set fitness elements to zero where they have not been selected for breeding:
        for i in xrange(len(populationGaps)):
            if populationGaps[i]:
                self.fitness[i] = 0

        return populationGaps

    def getParents(self):
        #only select parents that have a fitness value !=0 i.e. they have been selected to breed
        idx = [i for i,x in enumerate(self.fitness) if x != 0]
        #uses the uniform distribution: consider using a skewed distribution weighted towards the fitter members of the population (should cause quicker but possibly sub-optimal convergence?)
        np.random.shuffle(idx)
        parents = idx[:2]

        #using a chisqprob distrubution (exponentially decreasing) ie makes it more likely for fitter models to breed
        #parents = np.random.choice(idx,2,replace=False,p=self.Pweights[len(idx)])
        print 'parents:', parents, 'fitness', self.fitness
        father = self.population[parents[0]].getParams()
        mother = self.population[parents[1]].getParams()
        return father, mother

    #create child by combining attributes of the parents:
    def crossover(self, father, mother):
        params = {}
        fParams = father#.getParams()
        mParams = mother#.getParams()
        params['batch_size'] = np.random.choice([fParams['batch_size'], mParams['batch_size']])
        params['alpha'] = np.random.choice([fParams['alpha'], mParams['alpha']])
        params['dropout_prob'] = np.random.choice([fParams['dropout_prob'], mParams['dropout_prob']])
        params['optimizer_type'] = np.random.choice([fParams['optimizer_type'], mParams['optimizer_type']])
        params['depth'] = { '0': 1#grayscale
                            ,'1': np.random.choice([fParams['depth']['1'], mParams['depth']['1']])
                            ,'2': np.random.choice([fParams['depth']['2'], mParams['depth']['2']])
                            ,'3': np.random.choice([fParams['depth']['3'], mParams['depth']['3']])
                            ,'4': np.random.choice([fParams['depth']['4'], mParams['depth']['4']])
                            }
        params['nodes'] = { 'fc1': np.random.choice([fParams['nodes']['fc1'], mParams['nodes']['fc1']])
                            ,'fc2': np.random.choice([fParams['nodes']['fc2'], mParams['nodes']['fc2']])
                            ,'fc3': np.random.choice([fParams['nodes']['fc3'], mParams['nodes']['fc3']])
                            }
        model = convModel(self.const_params)
        model.setParams(params)
        return model

    #mutate genes to inroduce a random element into the population
    def mutate(self, child):
        params = child.getParams()
        #replace nMutatue number of attributes with a random choice from the range of acceptable values
        for _ in xrange(self.nMutate):
            key = np.random.choice(params.keys())
            if key not in ['depth', 'nodes']:
                #categorical variables, eg optimizer_type, have no numerical "closeness" so choose at random
                if key == 'optimizer_type':
                    params[key] = np.random.choice(self.varRange[key])
                #numerical variables have "closeness" so rather mutate to a value 1 larger or smaller
                else:
                    value = params[key]
                    idx = self.varRange[key].index(value)
                    idx = max(min(0,np.random.choice([idx-1, idx+1])),len(self.varRange[key])-1)
                    params[key] = self.varRange[key][idx]
            else:
                subKey = np.random.choice(params[key].keys())
                #numerical variables have "closeness" so rather mutate to a value 1 larger or smaller
                value = params[key][subKey]
                idx = self.varRange[key].index(value)
                idx = max(min(0,np.random.choice([idx-1, idx+1])),len(self.varRange[key])-1)
                params[key][subKey] = self.varRange[key][idx]
        #update parameters
        child.setParams(params)
        return child

    def displayFitnessHistory(self, show_fig=True):
        bestFitness = []

        #print the fitness history
        print '\n\nFitness History:'
        for i in xrange(self.nGenerations):
            print 'Gen ', i, ' fitness: ', self.fitnessHistory[i]
            bestFitness.append(self.fitnessHistory[i][0])

        if show_fig:
            plt.plot(bestFitness)
            plt.show()
