# Overview #
This repo contains a genetically optimised convolutional neural network to classify car images as either "front", "back" or "interior". It is coded in Python using the Tensorflow library. The dataset was scraped from the website "cars.co.za" (the code for this is in another of my repos: https://bitbucket.org/GeoffreyClark/cars_scraper).

# How to setup the code #
* Pull this repository to your local machine using git (git clone https://bitbucket.org/GeoffreyClark/classify_car_images)
* Setup a virtual environment (http://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html)
* Install the requirements (pip install -r requirements.txt)
* Install tensorflow. It is generally better to install from source. (https://www.tensorflow.org/install/)

# How to get the data #
Setup my other repository: https://bitbucket.org/GeoffreyClark/cars_scraper and scrape using the spider "NewCarsImageSpider".

# The three important Scripts that run the magic #
## processData.py ##
This takes the images that you scraped and turns them into grayscale matrices (you can experiment and use rgb colour). It also creates corresponding labels matrices and splits the data into training and test components capable of n-fold validation.

## nFoldValidation.py ##
This performs n-fold validation on the convolutional neural network. There are two execution options: either you can define manual parameters or you can load parameters that you have fine-tuned using the genetic algorithm.

## run_genetic_algorithm.py ##
As the name suggests, this executes the genetic algorithm (https://en.wikipedia.org/wiki/Genetic_algorithm). In the script you can specify permissable ranges of parameters values for certain of the hyperparameters. The script will also save the algorithm's final optimised parameters to disk for reuse.

# The two important Classes that contain the magic #
## model_class ##
This contains the convolutional neural network class. It takes in a dictionary of constant parameters on initialisation such as height and width of image. It also takes a set of variable parameters as inputs - these are the hyperparameters that will be tuned by the genetic algorithm. These parameters can also be retrieved from the class. It has a function to intialise the computational graph and also to unload it (to free up memory). It also has a training function which can either be accessed directly or via the nFoldValidation function.

## GA_class ##
This contains the genetic algorithm (GA) class. It implements the GA using n-fold validation as the fitness function. It uses crossover and mutation to breed new generations. At present it has a stopping criteria of simply reaching a fixed number of generations.
