import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

def accuracy(predictions,labels):
    return (100 * np.sum(np.argmax(predictions,1) == np.argmax(labels,1))
        /predictions.shape[0])

class convModel():
    def __init__(self, const_params):
        self.height = const_params['height']
        self.width = const_params['width']
        self.nLabels = const_params['nLabels']
        self.patch_size = const_params['patch_size']
        self.stride = const_params['stride']
        self.pstride = const_params['pstride']
        self.test_size = const_params['test_size']
        self.channels = const_params['channels']

    #Will use this to continually update the parameters for each
    def setParams(self, params):
        self.batch_size = params['batch_size']
        self.alpha = params['alpha']
        self.dropout_prob = params['dropout_prob']
        self.optimizer_type = params['optimizer_type']
        self.depth = params['depth']
        self.nodes = params['nodes']

    def getParams(self):
        params = {}
        params['batch_size'] = self.batch_size
        params['alpha'] = self.alpha
        params['dropout_prob'] = self.dropout_prob
        params['optimizer_type'] = self.optimizer_type
        params['depth'] = self.depth
        params['nodes'] = self.nodes
        return params

    #the graph & data appears to take up a lot of memory: so unload it from memory after training
    def unloadGrpah(self):
        del self.graph
        del self.tf_train_dataset
        del self.tf_train_labels
        del self.tf_test_dataset
        del self.loss
        del self.optimizer
        del self.train_prediction
        del self.test_prediction

    def setGraph(self):
        self.graph = tf.Graph()

        with self.graph.as_default():
            #data
            self.tf_train_dataset = tf.placeholder(tf.float32,
                shape=(self.batch_size,self.width,self.height,self.channels))
            self.tf_train_labels = tf.placeholder(tf.float32, shape=(self.batch_size,self.nLabels))
            self.tf_test_dataset = tf.placeholder(tf.float32,
                shape=(self.test_size,self.width,self.height,self.channels))

            #Variables
                #Use specific seed: We want to be able to replicate the results afterwards
                #and don't want the randomness of the weights and biases to interfere witht he results
            weights = {
                '1': tf.Variable(tf.truncated_normal(
                    [self.patch_size,self.patch_size,self.channels,self.depth['1']],seed=10))
                ,'2': tf.Variable(tf.truncated_normal(
                    [self.patch_size,self.patch_size,self.depth['1'],self.depth['2']],seed=10))
                ,'3': tf.Variable(tf.truncated_normal(
                    [self.patch_size,self.patch_size,self.depth['2'],self.depth['3']],seed=10))
                ,'4': tf.Variable(tf.truncated_normal(
                    [self.patch_size,self.patch_size,self.depth['3'],self.depth['4']],seed=10))
                ,'fc1': tf.Variable(tf.truncated_normal(
                    [self.width // ((self.stride**4) * (self.pstride**3)) * self.height
                    // ((self.stride**4) * (self.pstride**2)) * self.depth['4'], self.nodes['fc1']],seed=10))
                ,'fc2': tf.Variable(tf.truncated_normal(
                    [self.nodes['fc1'],self.nodes['fc2']],seed=10))
                ,'fc3': tf.Variable(tf.truncated_normal(
                    [self.nodes['fc2'],self.nodes['fc3']],seed=10))
                ,'fc4': tf.Variable(tf.truncated_normal(
                    [self.nodes['fc3'],self.nLabels],seed=10))
            }
            biases = {
                '1': tf.Variable(tf.zeros(self.depth['1']))
                ,'2': tf.Variable(tf.constant(1.0,shape=[self.depth['2']]))
                ,'3': tf.Variable(tf.constant(1.0,shape=[self.depth['3']]))
                ,'4': tf.Variable(tf.constant(1.0,shape=[self.depth['4']]))
                ,'fc1': tf.Variable(tf.constant(1.0,shape=[self.nodes['fc1']]))
                ,'fc2': tf.Variable(tf.constant(1.0,shape=[self.nodes['fc2']]))
                ,'fc3': tf.Variable(tf.constant(1.0,shape=[self.nodes['fc3']]))
                ,'fc4': tf.Variable(tf.constant(1.0,shape=[self.nLabels]))
            }
            #control drop-out with this - want to make <1 in training and 1 during predicting
            self.keep_prob = tf.placeholder(tf.float32, name="dropout_prob")

            # Model.
            def model(data):
                conv = tf.nn.conv2d(data, weights['1'], [1, self.stride, self.stride, 1], padding='SAME')
                hidden = tf.nn.relu(conv + biases['1'])
                pool = tf.nn.max_pool(hidden, ksize=[1,self.pstride, self.pstride,1], strides=[1,self.pstride,self.pstride,1],padding="SAME")
                conv = tf.nn.conv2d(pool, weights['2'], [1, self.stride, self.stride, 1], padding='SAME')
                hidden = tf.nn.relu(conv + biases['2'])
                pool = tf.nn.max_pool(hidden, ksize=[1,self.pstride, self.pstride,1], strides=[1,self.pstride,self.pstride,1],padding="SAME")
                conv = tf.nn.conv2d(pool, weights['3'], [1, self.stride, self.stride, 1], padding='SAME')
                hidden = tf.nn.relu(conv + biases['3'])
                pool = hidden
                conv = tf.nn.conv2d(pool, weights['4'], [1, self.stride, self.stride, 1], padding='SAME')
                hidden = tf.nn.relu(conv + biases['4'])
                #This pooling layer only pool over width (which is double height) - makes "image" square
                pool = tf.nn.max_pool(hidden, ksize=[1,self.pstride,1,1], strides=[1,self.pstride,1,1],padding="SAME")
                #link to fully connected layer
                shape = pool.get_shape().as_list()
                reshape = tf.reshape(pool, [shape[0], shape[1] * shape[2] * shape[3]])
                hidden = tf.nn.relu(tf.matmul(reshape, weights['fc1']) + biases['fc1'])
                hidden = tf.nn.dropout(hidden, self.keep_prob)
                hidden = tf.nn.relu(tf.matmul(hidden, weights['fc2']) + biases['fc2'])
                hidden = tf.nn.dropout(hidden, self.keep_prob)
                hidden = tf.nn.relu(tf.matmul(hidden, weights['fc3']) + biases['fc3'])
                hidden = tf.nn.dropout(hidden, self.keep_prob)
                return tf.matmul(hidden, weights['fc4']) + biases['fc4']

            # Training computation.
            logits = model(self.tf_train_dataset)
            self.loss = tf.reduce_mean(
              tf.nn.softmax_cross_entropy_with_logits(labels=self.tf_train_labels, logits=logits))

            # Optimizer.
            self.optimizer = self.optimizer_type(learning_rate=self.alpha).minimize(self.loss)

            # Predictions for the training, validation, and test data.
            self.train_prediction = tf.nn.softmax(logits)
            self.test_prediction = tf.nn.softmax(model(self.tf_test_dataset))

            # Add ops to save and restore all the variables.
            self.saver = tf.train.Saver()

    def train(self, train_data, train_labels, test_data, test_labels, epochs=10001, verbose=True, restore_model=False, save_model=False, show_fig=False):

        start_time = datetime.now()

        with tf.Session(graph=self.graph) as session:

            if restore_model and verbose:
                saver.restore(self.session, "chkpt/model.ckpt")
                print 'Model Restored'
            elif verbose:
                tf.global_variables_initializer().run()
                print 'Initialized'

            losses = []
            for step in xrange(epochs):
                offset = (step * self.batch_size) % (train_labels.shape[0] - self.batch_size)
                batch_data = train_data[offset:(offset + self.batch_size), :, :, :]
                batch_labels = train_labels[offset:(offset + self.batch_size), :]
                feed_dict = {self.tf_train_dataset : batch_data, self.tf_train_labels : batch_labels, self.keep_prob : self.dropout_prob}
                _, l, predictions = session.run(
                  [self.optimizer, self.loss, self.train_prediction], feed_dict=feed_dict)
                losses.append(l)
            if (step % 500 == 0 and verbose):
                print('Minibatch loss at step %d: %f' % (step, l))
                print('Minibatch accuracy: %.1f%%' % accuracy(predictions, batch_labels))

            feed_dict = {self.tf_test_dataset:test_data, self.keep_prob:1}
            testAccuracy = accuracy(self.test_prediction.eval(feed_dict=feed_dict), test_labels)
            if verbose:
                print('Test accuracy: %.1f%%' % testAccuracy)

            # Save the variables to disk.
            if save_model and verbose:
                save_path = self.saver.save(session, "chkpt/model.ckpt")
                print("Model saved in file: %s" % save_path)

        if verbose:
            print 'Training time: ', datetime.now() - start_time

        if show_fig:
            plt.plot(losses)
            plt.show()
        #this is what we want as the result of the training - the test accuracy
        return testAccuracy

    def kFoldValidation(self, train_data_dict, train_labels_dict, test_data_dict, test_labels_dict, K=5, epochs=6001):
        testAccuracy = np.zeros(K)
        self.setGraph()
        for i in range(K):
            testAccuracy[i] = self.train(train_data_dict[i], train_labels_dict[i], test_data_dict[i], test_labels_dict[i], epochs=epochs)
        #free up memory
        self.unloadGrpah()
        return testAccuracy.mean()
